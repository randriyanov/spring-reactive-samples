package com.ra.reactive.samples.intems.commons.service.leaves;

import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.persist.leaves.LeavesAndVacations;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IntemsUserLeavesCommonsService {

    public LeavesAndVacations mapToLeavesEntity(IntemsLeavesRequestDto requestDto) {
        LeavesAndVacations leavesAndVacations =new LeavesAndVacations();
        leavesAndVacations.setUserId(requestDto.getUserId());
        leavesAndVacations.setAbsence(5);
        leavesAndVacations.setSickLeaves(20);
        leavesAndVacations.setVacationDays(20);
        return leavesAndVacations;
    }

    public IntemsLeavesResponseDto toLeavesResponseDto(LeavesAndVacations leavesAndVacations) {
        return new IntemsLeavesResponseDto(leavesAndVacations.getId(), leavesAndVacations.getAbsence(),
                leavesAndVacations.getVacationDays(), leavesAndVacations.getSickLeaves(), leavesAndVacations.getUserId());
    }
}
