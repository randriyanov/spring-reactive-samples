package com.ra.reactive.samples.intems.commons.controller.user;

import com.ra.reactive.samples.intems.commons.service.user.IntemsUserService;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("basic/user")
@RequiredArgsConstructor
public class IntemsUserBasicController {

    private final IntemsUserService userService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<IntemsUserResponseDto> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return ResponseEntity.ok(userService.responseDto(requestDto));
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public ResponseEntity<List<IntemsUserResponseDto>> getUsersByLocation(@PathVariable String locationAddress) {
        return ResponseEntity.ok(userService.getUsersByLocation(locationAddress));
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public ResponseEntity<List<IntemsUserResponseDto>> getAllUsersLastNameFirstName(@PathVariable String firstName,
                                                                                    @PathVariable String lastName) {
        return ResponseEntity.ok(userService.getUsersByFirstNameLastName(firstName, lastName));
    }
}
