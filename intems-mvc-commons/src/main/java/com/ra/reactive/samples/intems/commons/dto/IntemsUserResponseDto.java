package com.ra.reactive.samples.intems.commons.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IntemsUserResponseDto {
    private Long id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String position;
    @JsonProperty("office_location")
    private String officeLocation;
    @JsonProperty("work_email")
    private String workEmail;
    @JsonProperty("about_me")
    private String aboutMe;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("leaves")
    private IntemsLeavesResponseDto leavesResponseDto;
}
