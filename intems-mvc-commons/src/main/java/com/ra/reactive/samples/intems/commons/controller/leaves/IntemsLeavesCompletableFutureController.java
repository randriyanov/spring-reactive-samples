package com.ra.reactive.samples.intems.commons.controller.leaves;

import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Controller
@RequestMapping("complete/leaves")
@RequiredArgsConstructor
public class IntemsLeavesCompletableFutureController {

    private final IntemsUserLeavesService leavesService;
    private final ThreadPoolTaskExecutor leavesTaskExecutor;

    @PostMapping
    @ResponseBody
    public CompletionStage<ResponseEntity<IntemsLeavesResponseDto>> createUser(@RequestBody IntemsLeavesRequestDto requestDto) {
        return CompletableFuture.supplyAsync(() -> leavesService.createLeaves(requestDto), leavesTaskExecutor)
                .thenApplyAsync(ResponseEntity::ok);
    }

    @GetMapping("/user")
    @ResponseBody
    public CompletionStage<ResponseEntity<List<IntemsLeavesResponseDto>>> getLeavesByIds(@RequestParam List<Long> ids) {
        return CompletableFuture.supplyAsync(() -> leavesService.getLeavesByUsersId(ids), leavesTaskExecutor)
                .thenApplyAsync(ResponseEntity::ok);
    }
}
