package com.ra.reactive.samples.intems.commons.controller.leaves;


import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@Controller
@RequestMapping("future/leaves")
@RequiredArgsConstructor
public class IntemsLeavesBasicFutureController {

    private final IntemsUserLeavesService leavesService;

    @PostMapping
    @ResponseBody
    public Callable<ResponseEntity<IntemsLeavesResponseDto>> createUser(@RequestBody IntemsLeavesRequestDto requestDto) {
        return () -> ResponseEntity.ok(leavesService.createLeaves(requestDto));
    }

    @GetMapping("/user")
    @ResponseBody
    public Callable<ResponseEntity<List<IntemsLeavesResponseDto>>> getLeavesByIds(@RequestParam List<Long> ids) {
        return () -> ResponseEntity.ok(leavesService.getLeavesByUsersId(ids));
    }
}
