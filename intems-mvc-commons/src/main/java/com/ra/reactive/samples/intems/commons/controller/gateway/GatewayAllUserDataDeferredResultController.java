package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("deferred/gateway")
public class GatewayAllUserDataDeferredResultController {
    private final Logger logger = LoggerFactory.getLogger(GatewayAllUserDataDeferredResultController.class);

    private final GatewayUserService userService;
    private final ThreadPoolTaskExecutor taskExecutor;

    @PostMapping
    @ResponseBody
    public DeferredResult<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        DeferredResult<ResponseEntity<IntemsUserResponseDto>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in create user.")));
        deferredResult.onError(throwable -> logger.error("Get create user callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.createUser(requestDto, "deferred")));
        });
        return deferredResult;
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in user location.")));
        deferredResult.onError(throwable -> logger.error("Get all user by location callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.getUsers(locationAddress, "deferred")));
        });
        return deferredResult;
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in user get name.")));
        deferredResult.onError(throwable -> logger.error("Get create user callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.getUsersByMatchedFirstLastName(firstName, lastName, "deferred")));
        });
        return deferredResult;
    }
}
