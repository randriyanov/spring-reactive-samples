package com.ra.reactive.samples.intems.commons.controller.user;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.user.IntemsUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("list/user")
@RequiredArgsConstructor
public class IntemsUserBasicListenableFutureController {

    private final IntemsUserService userService;
    private final AsyncListenableTaskExecutor taskExecutor;

    @PostMapping
    @ResponseBody
    public ListenableFuture<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return taskExecutor.submitListenable(() -> ResponseEntity.ok(userService.responseDto(requestDto)));
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public ListenableFuture<ResponseEntity<List<IntemsUserResponseDto>>> getUsersByLocation(@PathVariable String locationAddress) {
        return taskExecutor.submitListenable(() -> ResponseEntity.ok(userService.getUsersByLocation(locationAddress)));
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public ListenableFuture<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersLastNameFirstName(@PathVariable String firstName,
                                                                                              @PathVariable String lastName) {
        return taskExecutor.submitListenable(() -> ResponseEntity.ok(userService.getUsersByFirstNameLastName(firstName, lastName)));
    }
}
