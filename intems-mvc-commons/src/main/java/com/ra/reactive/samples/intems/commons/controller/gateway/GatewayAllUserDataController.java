package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("basic/gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataController {

    private final GatewayUserService userService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<IntemsUserResponseDto> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return ResponseEntity.ok(userService.createUser(requestDto, "basic"));
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public ResponseEntity<List<IntemsUserResponseDto>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        return ResponseEntity.ok(userService.getUsers(locationAddress, "basic"));
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public ResponseEntity<List<IntemsUserResponseDto>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        return ResponseEntity.ok(userService.getUsersByMatchedFirstLastName(firstName, lastName, "basic"));
    }
}
