package com.ra.reactive.samples.intems.commons.controller.user;

import com.ra.reactive.samples.intems.commons.controller.gateway.GatewayAllUserDataDeferredResultController;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.user.IntemsUserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@Controller
@RequestMapping("deferred/user")
@RequiredArgsConstructor
public class IntemsUserDeferredResultController {
    private final Logger logger = LoggerFactory.getLogger(GatewayAllUserDataDeferredResultController.class);

    private final IntemsUserService userService;
    private final ThreadPoolTaskExecutor taskExecutor;

    @PostMapping
    @ResponseBody
    public DeferredResult<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        DeferredResult<ResponseEntity<IntemsUserResponseDto>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in create user.")));
        deferredResult.onError(throwable -> logger.error("Get create user callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.responseDto(requestDto)));
        });
        return deferredResult;
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> getUsersByLocation(@PathVariable String locationAddress) {
        DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in get user by location.")));
        deferredResult.onError(throwable -> logger.error("Get user by location callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.getUsersByLocation(locationAddress)));
        });
        return deferredResult;
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersLastNameFirstName(@PathVariable String firstName,
                                                                                                      @PathVariable String lastName) {
        DeferredResult<ResponseEntity<List<IntemsUserResponseDto>>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in get user by last name first name.")));
        deferredResult.onError(throwable -> logger.error("Get user by last name first name callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(userService.getUsersByFirstNameLastName(firstName, lastName)));
        });
        return deferredResult;
    }
}
