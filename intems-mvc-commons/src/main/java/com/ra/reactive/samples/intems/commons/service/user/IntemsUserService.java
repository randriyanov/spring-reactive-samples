package com.ra.reactive.samples.intems.commons.service.user;

import com.ra.reactive.samples.intems.commons.repository.user.IntemsUserRepository;
import com.ra.reactive.samples.intems.commons.persist.Position;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.persist.user.IntemsUser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IntemsUserService {

    private final IntemsUserRepository userRepository;

    @Transactional
    public IntemsUserResponseDto responseDto(IntemsUserRequestDto requestDto) {
        IntemsUser intemsUser = new IntemsUser();
        intemsUser.setAboutMe(requestDto.getAboutMe());
        intemsUser.setFirstName(requestDto.getFirstName());
        intemsUser.setLastName(requestDto.getLastName());
        intemsUser.setOfficeLocation(requestDto.getOfficeLocation());
        intemsUser.setWorkEmail(requestDto.getWorkEmail());
        intemsUser.setPhoneNumber(requestDto.getPhoneNumber());
        intemsUser.setOfficeLocation(requestDto.getOfficeLocation());
        intemsUser.setPosition(Position.JAVA_LEAD_SOFTWARE_ENGINEER);
        userRepository.save(intemsUser);
        return new IntemsUserResponseDto(intemsUser.getId(), requestDto.getFirstName(),
                requestDto.getLastName(), intemsUser.getPosition().positionName, requestDto.getOfficeLocation(),
                requestDto.getWorkEmail(), requestDto.getAboutMe(), requestDto.getPhoneNumber(), null);
    }

    public List<IntemsUserResponseDto> getUsersByLocation(String location) {
        return userRepository.findAllByOfficeLocation(location)
                .orElse(List.of())
                .stream().map(this::mapToDto)
                .collect(Collectors.toList());
    }

    public List<IntemsUserResponseDto> getUsersByFirstNameLastName(String firstName, String lastName) {
        return userRepository.findTop100ByFirstNameOrLastName(firstName, lastName)
                .orElse(List.of())
                .stream().map(this::mapToDto)
                .collect(Collectors.toList());
    }

    private IntemsUserResponseDto mapToDto(IntemsUser intemsUser) {
        return new IntemsUserResponseDto(intemsUser.getId(), intemsUser.getFirstName(),
                intemsUser.getLastName(), intemsUser.getPosition().positionName, intemsUser.getOfficeLocation(),
                intemsUser.getWorkEmail(), intemsUser.getAboutMe(), intemsUser.getPhoneNumber(), null);
    }
}
