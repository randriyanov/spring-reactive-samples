package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.WebAsyncTask;

import java.util.List;
import java.util.concurrent.Callable;

@Component
@RequestMapping("webtask/gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataWebAsyncTask {

    private final AsyncTaskExecutor taskExecutor;
    private final GatewayUserService userService;

    @PostMapping
    @ResponseBody
    public WebAsyncTask<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        Callable<ResponseEntity<IntemsUserResponseDto>> callable = () -> ResponseEntity.ok(userService.createUser(requestDto, "webtask"));
        return new WebAsyncTask<>(10000L, taskExecutor, callable);
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public WebAsyncTask<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        Callable<ResponseEntity<List<IntemsUserResponseDto>>> callable = () -> ResponseEntity.ok(userService.getUsers(locationAddress, "webtask"));
        return new WebAsyncTask<>(10000L, taskExecutor, callable);
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public WebAsyncTask<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        Callable<ResponseEntity<List<IntemsUserResponseDto>>> callable =  () -> ResponseEntity.ok(userService.getUsersByMatchedFirstLastName(firstName, lastName, "webtask"));
        return new WebAsyncTask<>(10000L, taskExecutor, callable);
    }
}
