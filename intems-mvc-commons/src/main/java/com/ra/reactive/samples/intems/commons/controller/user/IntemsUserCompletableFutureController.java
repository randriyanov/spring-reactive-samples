package com.ra.reactive.samples.intems.commons.controller.user;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.user.IntemsUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Controller
@RequestMapping("complete/user")
@RequiredArgsConstructor
public class IntemsUserCompletableFutureController {

    private final IntemsUserService userService;
    private final ThreadPoolTaskExecutor userTaskExecutor;

    @PostMapping
    @ResponseBody
    public CompletionStage<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return CompletableFuture.supplyAsync(
                () -> userService.responseDto(requestDto), userTaskExecutor)
                .thenApplyAsync(ResponseEntity::ok);
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public CompletionStage<ResponseEntity<List<IntemsUserResponseDto>>> getUsersByLocation(@PathVariable String locationAddress) {
        return CompletableFuture.supplyAsync(() -> userService.getUsersByLocation(locationAddress), userTaskExecutor)
                .thenApplyAsync(ResponseEntity::ok);
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public CompletionStage<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersLastNameFirstName(@PathVariable String firstName,
                                                                                    @PathVariable String lastName) {
        return CompletableFuture.supplyAsync(() -> userService.getUsersByFirstNameLastName(firstName, lastName), userTaskExecutor)
                .thenApplyAsync(ResponseEntity::ok);
    }
}
