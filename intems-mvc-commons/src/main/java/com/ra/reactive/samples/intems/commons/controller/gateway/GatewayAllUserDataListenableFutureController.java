package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("list/gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataListenableFutureController {

    private final AsyncListenableTaskExecutor taskExecutor;
    private final GatewayUserService userService;

    @PostMapping
    @ResponseBody
    public ListenableFuture<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return taskExecutor.submitListenable(() -> ResponseEntity.ok(userService.createUser(requestDto, "list")));
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public ListenableFuture<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        return taskExecutor.submitListenable(() -> ResponseEntity.ok(userService.getUsers(locationAddress, "list")));
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public ListenableFuture<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        return taskExecutor.submitListenable(
                () -> ResponseEntity.ok(userService.getUsersByMatchedFirstLastName(firstName, lastName, "list")));
    }
}
