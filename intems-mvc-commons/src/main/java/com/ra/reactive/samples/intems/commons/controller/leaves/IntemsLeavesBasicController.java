package com.ra.reactive.samples.intems.commons.controller.leaves;

import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("basic/leaves")
@RequiredArgsConstructor
public class IntemsLeavesBasicController {

    private final IntemsUserLeavesService leavesService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<IntemsLeavesResponseDto> createUser(@RequestBody IntemsLeavesRequestDto requestDto) {
        return ResponseEntity.ok(leavesService.createLeaves(requestDto));
    }

    @GetMapping("/user")
    @ResponseBody
    public ResponseEntity<List<IntemsLeavesResponseDto>> getLeavesByIds(@RequestParam List<Long> ids) {
        return ResponseEntity.ok(leavesService.getLeavesByUsersId(ids));
    }
}
