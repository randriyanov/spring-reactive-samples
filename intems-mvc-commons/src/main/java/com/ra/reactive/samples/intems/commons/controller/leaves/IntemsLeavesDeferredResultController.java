package com.ra.reactive.samples.intems.commons.controller.leaves;

import com.ra.reactive.samples.intems.commons.controller.gateway.GatewayAllUserDataDeferredResultController;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

@Controller
@RequestMapping("deferred/leaves")
@RequiredArgsConstructor
public class IntemsLeavesDeferredResultController {
    private final Logger logger = LoggerFactory.getLogger(GatewayAllUserDataDeferredResultController.class);

    private final IntemsUserLeavesService leavesService;
    private final ThreadPoolTaskExecutor taskExecutor;

    @PostMapping
    @ResponseBody
    public DeferredResult<ResponseEntity<IntemsLeavesResponseDto>> createLeave(@RequestBody IntemsLeavesRequestDto requestDto) {
        DeferredResult<ResponseEntity<IntemsLeavesResponseDto>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in create leaves for user.")));
        deferredResult.onError(throwable -> logger.error("create leave callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(leavesService.createLeaves(requestDto)));
        });
        return deferredResult;
    }

    @GetMapping("/user")
    @ResponseBody
    public DeferredResult<ResponseEntity<List<IntemsLeavesResponseDto>>> getLeavesByIds(@RequestParam List<Long> ids) {
        DeferredResult<ResponseEntity<List<IntemsLeavesResponseDto>>> deferredResult = new DeferredResult<>(10000L);
        deferredResult.onTimeout(() -> deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("Request timeout occurred in get leaves by user id.")));
        deferredResult.onError(throwable -> logger.error("Get leaves by user id callback {}", throwable.getMessage()));
        taskExecutor.execute(() -> {
            deferredResult.setResult(ResponseEntity.ok(leavesService.getLeavesByUsersId(ids)));
        });
        return deferredResult;
    }


}
