package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayCompletionUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletionStage;

@Controller
@RequestMapping("complete/gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataCompletableFutureController {

    private final GatewayCompletionUserService userService;

    @PostMapping
    @ResponseBody
    public CompletionStage<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return userService.createUser(requestDto, "complete").thenApplyAsync(ResponseEntity::ok);
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public CompletionStage<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        return userService.getUsers(locationAddress, "complete").thenApplyAsync(ResponseEntity::ok);
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public CompletionStage<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        return userService.getUsersByMatchedFirstLastName(firstName, lastName, "complete")
                .thenApplyAsync(ResponseEntity::ok);
    }
}
