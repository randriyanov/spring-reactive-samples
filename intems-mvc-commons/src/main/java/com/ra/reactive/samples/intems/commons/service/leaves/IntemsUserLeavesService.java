package com.ra.reactive.samples.intems.commons.service.leaves;

import com.ra.reactive.samples.intems.commons.repository.leaves.IntemsUserLeavesRepository;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.persist.leaves.LeavesAndVacations;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class IntemsUserLeavesService {

    private final IntemsUserLeavesRepository userLeavesRepository;
    private final IntemsUserLeavesCommonsService commonsService;

    @Transactional
    public IntemsLeavesResponseDto createLeaves(IntemsLeavesRequestDto requestDto) {
        LeavesAndVacations leavesAndVacations = commonsService.mapToLeavesEntity(requestDto);
        userLeavesRepository.save(leavesAndVacations);
        return new IntemsLeavesResponseDto(leavesAndVacations.getId(), leavesAndVacations.getAbsence(),
                leavesAndVacations.getVacationDays(), leavesAndVacations.getSickLeaves(), requestDto.getUserId());
    }

    public List<IntemsLeavesResponseDto> getLeavesByUsersId(List<Long> usersId) {
        return userLeavesRepository.findAllByUserIdIn(usersId)
                .orElse(List.of())
                .stream()
                .map(commonsService::toLeavesResponseDto).collect(Collectors.toList());
    }

}
