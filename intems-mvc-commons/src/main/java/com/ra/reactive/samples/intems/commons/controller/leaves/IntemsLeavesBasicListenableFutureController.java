package com.ra.reactive.samples.intems.commons.controller.leaves;

import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("list/leaves")
@RequiredArgsConstructor
public class IntemsLeavesBasicListenableFutureController {

    private final IntemsUserLeavesService leavesService;
    private final AsyncListenableTaskExecutor taskExecutor;

    @PostMapping
    @ResponseBody
    public ListenableFuture<ResponseEntity<IntemsLeavesResponseDto>> createUser(@RequestBody IntemsLeavesRequestDto requestDto) {
        return taskExecutor.submitListenable(
                () -> ResponseEntity.ok(leavesService.createLeaves(requestDto)));
    }

    @GetMapping("/user")
    @ResponseBody
    public ListenableFuture<ResponseEntity<List<IntemsLeavesResponseDto>>> getLeavesByIds(@RequestParam List<Long> ids) {
        return taskExecutor.submitListenable(
                () -> ResponseEntity.ok(leavesService.getLeavesByUsersId(ids)));
    }
}
