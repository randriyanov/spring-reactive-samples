package com.ra.reactive.samples.intems.commons.service.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsLeavesResponseDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.exceptions.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class GatewayCompletionUserService {

    @Value("${user.service.url}")
    private String userUrl;

    @Value("${leaves.service.url}")
    private String leavesUrl;

    private final RestTemplate userRestTemplate;
    private final RestTemplate leavesRestTemplate;
    private final ThreadPoolTaskExecutor gatewayTaskExecutor;

    public CompletionStage<IntemsUserResponseDto> createUser(IntemsUserRequestDto requestDto, String path) {
        var userRequest = new HttpEntity<>(requestDto);
        var userResponseDto = new IntemsUserResponseDto();
        return CompletableFuture.supplyAsync(
                () -> userRestTemplate.postForEntity(userUrl + path + "/user", userRequest, IntemsUserResponseDto.class).getBody(), gatewayTaskExecutor)
                .thenAcceptAsync(dto -> mapIntemsUserResponseDto(dto, userResponseDto))
                .thenApplyAsync(aVoid -> {
                    var leavesRequest = new HttpEntity<>(new IntemsLeavesRequestDto(userResponseDto.getId()));
                    return leavesRestTemplate.postForEntity(leavesUrl + path + "/leaves", leavesRequest, IntemsLeavesResponseDto.class).getBody();
                }).thenApplyAsync(leaves -> {
                    userResponseDto.setLeavesResponseDto(leaves);
                    return userResponseDto;
                });
    }

    public CompletionStage<List<IntemsUserResponseDto>> getUsers(String officeLocation, String path) {
        return CompletableFuture.supplyAsync(() -> getUsersByLocation(officeLocation, path), gatewayTaskExecutor)
                .thenApplyAsync(dtos -> buildUserResponseWithLeaves(dtos, path));
    }

    public CompletionStage<List<IntemsUserResponseDto>> getUsersByMatchedFirstLastName(String firstName, String lastName, String path) {
        return CompletableFuture.supplyAsync(() -> getUsersByFirstLastName(firstName, lastName, path))
                .thenApplyAsync(dtos ->  buildUserResponseWithLeaves(dtos, path));
    }

    private void mapIntemsUserResponseDto(IntemsUserResponseDto from, IntemsUserResponseDto to) {
        to.setAboutMe(from.getAboutMe());
        to.setFirstName(from.getFirstName());
        to.setId(from.getId());
        to.setLastName(from.getLastName());
        to.setOfficeLocation(from.getOfficeLocation());
        to.setWorkEmail(from.getWorkEmail());
        to.setPhoneNumber(from.getPhoneNumber());
    }

    private List<IntemsUserResponseDto> buildUserResponseWithLeaves(IntemsUserResponseDto[] userResponseDtos, String path) {
        Map<Long, IntemsUserResponseDto> usersMap = Stream.of(userResponseDtos)
                .collect(
                        Collectors.toMap(IntemsUserResponseDto::getId, Function.identity(), (first, second) -> first));
        var leavesResponse = getUsersLeavesById(userResponseDtos, path);
        return Stream.of(leavesResponse).filter(leaves -> usersMap.containsKey(leaves.getUserId()))
                .map(leavesDto -> {
                    IntemsUserResponseDto userDto = usersMap.get(leavesDto.getUserId());
                    userDto.setLeavesResponseDto(leavesDto);
                    return userDto;
                }).collect(Collectors.toList());
    }

    private IntemsUserResponseDto[] getUsersByFirstLastName(String firstName, String lastName, String path) {
        var userRequestUrl = userUrl + path + "/user/name/" + firstName + "/" + lastName;
        try {
            return userRestTemplate.getForEntity(userRequestUrl, IntemsUserResponseDto[].class).getBody();
        } catch (Exception e) {
            throw new UserNotFoundException();
        }
    }

    private IntemsUserResponseDto[] getUsersByLocation(String officeLocation, String path) {
        var userRequestUrl = userUrl + path + "/user/location/" + officeLocation;
        try {
            return userRestTemplate.getForEntity(userRequestUrl, IntemsUserResponseDto[].class).getBody();
        } catch (Exception e) {
            throw new UserNotFoundException();
        }
    }

    private IntemsLeavesResponseDto[] getUsersLeavesById(IntemsUserResponseDto[] userResponseDtos, String path) {
        var userLeavesRequest = leavesUrl + path + "/leaves/user?" + "ids=" + userIds(userResponseDtos);
        try {
            return leavesRestTemplate.getForEntity(userLeavesRequest, IntemsLeavesResponseDto[].class).getBody();
        } catch (Exception e) {
            throw new UserNotFoundException();
        }
    }

    private String userIds(IntemsUserResponseDto[] userResponseDtos) {
        return Stream.of(userResponseDtos)
                .map(IntemsUserResponseDto::getId).map(Object::toString)
                .collect(Collectors.joining(","));
    }
}
