package com.ra.reactive.samples.intems.commons.controller.gateway;

import com.ra.reactive.samples.intems.commons.dto.IntemsUserRequestDto;
import com.ra.reactive.samples.intems.commons.dto.IntemsUserResponseDto;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@Controller
@RequestMapping("future/gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataFutureController {

    private final GatewayUserService userService;

    @PostMapping
    @ResponseBody
    public Callable<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return () -> ResponseEntity.ok(userService.createUser(requestDto, "future"));
    }

    @GetMapping("/location/{locationAddress}")
    @ResponseBody
    public Callable<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        return () -> ResponseEntity.ok(userService.getUsers(locationAddress, "future"));
    }

    @GetMapping("/name/{firstName}/{lastName}")
    @ResponseBody
    public Callable<ResponseEntity<List<IntemsUserResponseDto>>> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        return () -> ResponseEntity.ok(userService.getUsersByMatchedFirstLastName(firstName, lastName, "future"));
    }
}
