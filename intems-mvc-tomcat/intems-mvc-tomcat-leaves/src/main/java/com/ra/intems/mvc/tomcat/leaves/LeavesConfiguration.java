package com.ra.intems.mvc.tomcat.leaves;

import com.ra.reactive.samples.intems.commons.configuration.persistance.PersistenceConfiguration;
import com.ra.reactive.samples.intems.commons.controller.leaves.IntemsLeavesBasicController;
import com.ra.reactive.samples.intems.commons.repository.leaves.IntemsUserLeavesRepository;
import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesCommonsService;
import com.ra.reactive.samples.intems.commons.service.leaves.IntemsUserLeavesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan(basePackageClasses = {PersistenceConfiguration.class})
@EnableJpaRepositories("com.ra.reactive.samples.intems.commons.repository.leaves")
@PropertySource("classpath:application.properties")
@Configuration
public class LeavesConfiguration {

    @Bean
    @Autowired
    public IntemsUserLeavesService leavesService(IntemsUserLeavesCommonsService basicLeavesCommonsService,
                                                 IntemsUserLeavesRepository repository) {
        return new IntemsUserLeavesService(repository, basicLeavesCommonsService);
    }

    @Bean
    public IntemsUserLeavesCommonsService basicLeavesCommonsService() {
        return new IntemsUserLeavesCommonsService();
    }

    @Bean
    @Autowired
    public IntemsLeavesBasicController basicController(IntemsUserLeavesService leavesService) {
        return new IntemsLeavesBasicController(leavesService);
    }
}
