#!/bin/bash
./../gradlew clean build
TOMCAT_GATEWAY_PATH="/Users/randriinov/Downloads/intems-tomcat-8/apache-tomcat-gateway"
TOMCAT_USER_PATH="/Users/randriinov/Downloads/intems-tomcat-8/apache-tomcat-user"
TOMCAT_LEAVES_PATH="/Users/randriinov/Downloads/intems-tomcat-8/apache-tomcat-leaves"
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_191.jdk/Contents/Home
rm -rf "$TOMCAT_GATEWAY_PATH"/webapps/*
rm -rf "$TOMCAT_USER_PATH"/webapps/*
rm -rf "$TOMCAT_LEAVES_PATH"/webapps/*
cp $(pwd)/intems-mvc-tomcat-gateway/build/libs/intems-mvc-tomcat-gateway.war "$TOMCAT_GATEWAY_PATH"/webapps/
cp $(pwd)/intems-mvc-tomcat-user/build/libs/intems-mvc-tomcat-user.war "$TOMCAT_USER_PATH"/webapps/
cp $(pwd)/intems-mvc-tomcat-leaves/build/libs/intems-mvc-tomcat-leaves.war "$TOMCAT_LEAVES_PATH"/webapps/
sleep 1
sh "$TOMCAT_GATEWAY_PATH"/bin/startup.sh
sleep 1
sh "$TOMCAT_USER_PATH"/bin/startup.sh
sleep 1
sh "$TOMCAT_LEAVES_PATH"/bin/startup.sh