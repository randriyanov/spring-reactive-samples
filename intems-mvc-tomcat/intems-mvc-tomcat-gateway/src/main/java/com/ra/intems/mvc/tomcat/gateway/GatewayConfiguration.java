package com.ra.intems.mvc.tomcat.gateway;

import com.ra.reactive.samples.intems.commons.controller.gateway.GatewayAllUserDataController;
import com.ra.reactive.samples.intems.commons.service.gateway.GatewayUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@PropertySource("classpath:application.properties")
public class GatewayConfiguration {

    @Bean
    public RestTemplate userRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate leavesRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public GatewayUserService gatewayUserService(RestTemplate userRestTemplate, RestTemplate leavesRestTemplate) {
        return new GatewayUserService(userRestTemplate, leavesRestTemplate);
    }

    @Bean
    @Autowired
    public GatewayAllUserDataController gatewayAllUserDataController(GatewayUserService gatewayUserService) {
        return new GatewayAllUserDataController(gatewayUserService);
    }
}
