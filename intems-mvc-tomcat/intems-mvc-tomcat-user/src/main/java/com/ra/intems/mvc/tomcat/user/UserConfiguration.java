package com.ra.intems.mvc.tomcat.user;

import com.ra.reactive.samples.intems.commons.configuration.persistance.PersistenceConfiguration;
import com.ra.reactive.samples.intems.commons.controller.user.IntemsUserBasicController;
import com.ra.reactive.samples.intems.commons.repository.user.IntemsUserRepository;
import com.ra.reactive.samples.intems.commons.service.user.IntemsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan(basePackageClasses = {PersistenceConfiguration.class})
@EnableJpaRepositories("com.ra.reactive.samples.intems.commons.repository.user")
@PropertySource("classpath:application.properties")
@Configuration
public class UserConfiguration {

    @Bean
    @Autowired
    public IntemsUserBasicController basicController(IntemsUserService userService) {
        return new IntemsUserBasicController(userService);
    }

    @Bean
    @Autowired
    public IntemsUserService intemsUserService(IntemsUserRepository userRepository) {
        return new IntemsUserService(userRepository);
    }
}
