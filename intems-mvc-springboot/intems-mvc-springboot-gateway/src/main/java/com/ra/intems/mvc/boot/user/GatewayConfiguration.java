package com.ra.intems.mvc.boot.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class GatewayConfiguration {
    @Bean
    public RestTemplate userRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate leavesRestTemplate() {
        return new RestTemplate();
    }
}
