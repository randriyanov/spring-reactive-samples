package com.ra.intems.mvc.boot.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(scanBasePackages =
        {"com.ra.reactive.samples.intems.commons.service.gateway",
                "com.ra.reactive.samples.intems.commons.controller.gateway"},
scanBasePackageClasses = GatewayConfiguration.class)
public class ApplicationGateway {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationGateway.class, args);
    }

    @Bean
    @ConditionalOnProperty(value = "gateway.enable-thread-pool", havingValue = "true")
    WebMvcConfigurer configurer(){
        return new WebMvcConfigurer(){
            @Override
            public void configureAsyncSupport (AsyncSupportConfigurer configurer) {
                ThreadPoolTaskExecutor t = new ThreadPoolTaskExecutor();
                t.setCorePoolSize(100);
                t.setAllowCoreThreadTimeOut(true);
                t.setKeepAliveSeconds(120);
                t.setThreadNamePrefix("gateway-thread-prefix-");
                t.setThreadGroupName("gateway-thread-prefix-");
                t.initialize();
                configurer.setTaskExecutor(t);
            }
        };
    }

    @Bean
    @ConditionalOnProperty(value = "gateway.enable-custom-thread-pool", havingValue = "true")
    public ThreadPoolTaskExecutor gatewayTaskExecutor() {
        ThreadPoolTaskExecutor t = new ThreadPoolTaskExecutor();
        t.setCorePoolSize(100);
        t.setAllowCoreThreadTimeOut(true);
        t.setKeepAliveSeconds(120);
        t.setThreadNamePrefix("gateway-custom-thread-prefix-");
        t.setThreadGroupName("gateway-custom-prefix-");
        t.initialize();
        return t;
    }


    @Bean
    @ConditionalOnProperty(value = "gateway.enable-async-thread-pool", havingValue = "true")
    public AsyncListenableTaskExecutor taskExecutor () {
        SimpleAsyncTaskExecutor t = new SimpleAsyncTaskExecutor();
        t.setConcurrencyLimit(-1);
        t.setThreadGroupName("spring-gateway");
        return t;
    }
}
