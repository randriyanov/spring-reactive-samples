package com.ra.intems.mvc.boot.user;

import com.ra.reactive.samples.intems.commons.persist.user.IntemsUser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(scanBasePackages =
        {"com.ra.reactive.samples.intems.commons.service.user",
                "com.ra.reactive.samples.intems.commons.controller.user"})
@EnableJpaRepositories("com.ra.reactive.samples.intems.commons.repository.user")
@EntityScan(basePackageClasses = IntemsUser.class)
public class ApplicationUser {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationUser.class, args);
    }

    @Bean
    @ConditionalOnProperty(value = "users.enable-thread-pool", havingValue = "true")
    WebMvcConfigurer configurer(){
        return new WebMvcConfigurer(){
            @Override
            public void configureAsyncSupport (AsyncSupportConfigurer configurer) {
                ThreadPoolTaskExecutor t = new ThreadPoolTaskExecutor();
                t.setCorePoolSize(100);
                t.setAllowCoreThreadTimeOut(true);
                t.setKeepAliveSeconds(120);
                t.initialize();
                configurer.setTaskExecutor(t);
            }
        };
    }

    @Bean
    @ConditionalOnProperty(value = "users.enable-custom-thread-pool", havingValue = "true")
    public ThreadPoolTaskExecutor userTaskExecutor() {
        ThreadPoolTaskExecutor t = new ThreadPoolTaskExecutor();
        t.setCorePoolSize(100);
        t.setAllowCoreThreadTimeOut(true);
        t.setKeepAliveSeconds(120);
        t.setThreadNamePrefix("user-thread-prefix-");
        t.setThreadGroupName("user-thread-prefix-");
        t.initialize();
        return t;
    }

    @Bean
    @ConditionalOnProperty(value = "users.enable-async-thread-pool", havingValue = "true")
    public AsyncListenableTaskExecutor taskExecutor () {
        SimpleAsyncTaskExecutor t = new SimpleAsyncTaskExecutor();
        t.setConcurrencyLimit(-1);
        t.setThreadGroupName("spring-user-");
        t.setThreadNamePrefix("spring-user-");
        return t;
    }
}
