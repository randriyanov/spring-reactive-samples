#!/bin/bash
./../gradlew clean build
sleep 1
nohup java -jar intems-mvc-springboot-gateway/build/libs/intems-mvc-springboot-gateway.jar &
sleep 1
nohup java -jar intems-mvc-springboot-leaves/build/libs/intems-mvc-springboot-leaves.jar &
sleep 1
nohup java -jar intems-mvc-springboot-user/build/libs/intems-mvc-springboot-user.jar &