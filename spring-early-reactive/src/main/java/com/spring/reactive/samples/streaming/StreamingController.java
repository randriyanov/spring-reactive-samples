package com.spring.reactive.samples.streaming;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.OutputStream;

/*
In situations where we want to write raw streaming bytes to the output, our handler method should return an instance
of StreamingResponseBody instead of ResponseBodyEmitter.
ResponseBodyEmitter writes output message through HttpMessageConverter,
whereas, StreamingResponseBody writes directly to response OutputStream.
StreamingResponseBody is preferable when streaming videos or large files.
StreamingResponseBody is an interface with only one method
 */
@Controller
@RequestMapping("streaming")
public class StreamingController {

    @RequestMapping("/test")
    public ResponseEntity<StreamingResponseBody> handleRequest () {

        StreamingResponseBody responseBody = new StreamingResponseBody() {
            @Override
            public void writeTo (OutputStream out) throws IOException {
                for (int i = 0; i < 1000; i++) {
                    out.write((Integer.toString(i) + " - ")
                            .getBytes());
                    out.flush();
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

}
