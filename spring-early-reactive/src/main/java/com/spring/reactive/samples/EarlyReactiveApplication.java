package com.spring.reactive.samples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EarlyReactiveApplication {
    public static void main (String[] args) {
        SpringApplication.run(EarlyReactiveApplication.class, args);
    }
}
