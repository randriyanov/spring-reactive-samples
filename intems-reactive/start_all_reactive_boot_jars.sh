#!/bin/bash
./../gradlew clean build
sleep 1
nohup java -jar intems-reactive-gateway/build/libs/intems-reactive-gateway.jar &
sleep 1
nohup java -jar intems-reactive-leaves/build/libs/intems-reactive-leaves.jar &
sleep 1
nohup java -jar intems-reactive-user/build/libs/intems-reactive-user.jar &