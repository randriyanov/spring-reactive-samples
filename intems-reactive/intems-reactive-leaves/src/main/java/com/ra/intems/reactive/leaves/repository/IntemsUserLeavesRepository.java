package com.ra.intems.reactive.leaves.repository;

import com.ra.intems.reactive.leaves.entity.LeavesAndVacations;
import lombok.RequiredArgsConstructor;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class IntemsUserLeavesRepository {
    private String TEMPLATE = "SELECT * FROM leaves_vacations WHERE leaves_vacations.user_id IN (";

    private final DatabaseClient databaseClient;

    public Flux<LeavesAndVacations> retrieveLeavesByUserIds(List<Long> ids) {
        return databaseClient.execute(createQuery(ids)).as(LeavesAndVacations.class).fetch().all();
    }

    private String createQuery(List<Long> ids) {
        return TEMPLATE + ids.stream().map(Object::toString).collect(Collectors.joining(",")) + ")";
    }
}
