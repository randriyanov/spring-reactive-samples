package com.ra.intems.reactive.leaves.router;

import com.ra.intems.reactive.leaves.handler.LeavesHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class LeavesRouter {

    @Bean
    public RouterFunction<ServerResponse> routerAddress(LeavesHandlerFunction handlerFunction) {
        return route(POST("/leaves").and(accept(MediaType.APPLICATION_JSON)), handlerFunction::create)
                .andRoute(GET("/leaves/user").and(contentType(MediaType.APPLICATION_STREAM_JSON)), handlerFunction::getLeavesByIds);
    }

}
