package com.ra.intems.reactive.leaves;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationReactiveLeaves {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationReactiveLeaves.class, args);
    }
}
