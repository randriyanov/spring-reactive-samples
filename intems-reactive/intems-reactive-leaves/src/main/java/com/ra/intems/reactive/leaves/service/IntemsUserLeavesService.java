package com.ra.intems.reactive.leaves.service;

import com.ra.intems.reactive.leaves.dto.IntemsLeavesRequestDto;
import com.ra.intems.reactive.leaves.dto.IntemsLeavesResponseDto;
import com.ra.intems.reactive.leaves.entity.LeavesAndVacations;
import com.ra.intems.reactive.leaves.exception.LeavesNotFoundException;
import com.ra.intems.reactive.leaves.repository.IntemsUserLeavesReactiveRepository;
import com.ra.intems.reactive.leaves.repository.IntemsUserLeavesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
public class IntemsUserLeavesService {

    private final IntemsUserLeavesReactiveRepository userLeavesRepository;
    private final IntemsUserLeavesRepository repository;
    private final IntemsUserLeavesCommonsService commonsService;

    @Transactional
    public Mono<IntemsLeavesResponseDto> createLeaves(IntemsLeavesRequestDto requestDto) {
        LeavesAndVacations leavesAndVacations = commonsService.mapToLeavesEntity(requestDto);
        return userLeavesRepository.save(leavesAndVacations).map(commonsService::toLeavesResponseDto);
    }

    public Flux<IntemsLeavesResponseDto> getLeavesByUsersId(List<Long> usersId) {
        return repository.retrieveLeavesByUserIds(usersId)
                .map(commonsService::toLeavesResponseDto)
                .switchIfEmpty(Mono.error(new LeavesNotFoundException("Leaves not found for this ids'")));

    }

}
