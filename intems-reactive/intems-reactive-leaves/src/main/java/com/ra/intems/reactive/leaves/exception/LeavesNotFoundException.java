package com.ra.intems.reactive.leaves.exception;

public class LeavesNotFoundException extends RuntimeException {

    public LeavesNotFoundException(String message) {
        super(message);
    }
}
