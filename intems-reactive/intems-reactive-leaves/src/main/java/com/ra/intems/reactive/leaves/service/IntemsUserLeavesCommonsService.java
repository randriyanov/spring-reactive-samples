package com.ra.intems.reactive.leaves.service;

import com.ra.intems.reactive.leaves.dto.IntemsLeavesRequestDto;
import com.ra.intems.reactive.leaves.dto.IntemsLeavesResponseDto;
import com.ra.intems.reactive.leaves.entity.LeavesAndVacations;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IntemsUserLeavesCommonsService {

    public LeavesAndVacations mapToLeavesEntity(IntemsLeavesRequestDto requestDto) {
        LeavesAndVacations leavesAndVacations =new LeavesAndVacations();
        leavesAndVacations.setUserId(requestDto.getUserId());
        leavesAndVacations.setAbsence(5);
        leavesAndVacations.setSickLeaves(20);
        leavesAndVacations.setVacationDays(20);
        return leavesAndVacations;
    }

    public IntemsLeavesResponseDto toLeavesResponseDto(LeavesAndVacations leavesAndVacations) {
        return new IntemsLeavesResponseDto(leavesAndVacations.getId(), leavesAndVacations.getAbsence(),
                leavesAndVacations.getVacationDays(), leavesAndVacations.getSickLeaves(), leavesAndVacations.getUserId());
    }
}
