package com.ra.intems.reactive.leaves.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("leaves_vacations")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"userId"})
public class LeavesAndVacations {
    @Id
    private Long id;
    @Column("user_id")
    private Long userId;
    private int absence;
    @Column("vacation_days")
    private int vacationDays;
    @Column("sick_leaves")
    private int sickLeaves;
}
