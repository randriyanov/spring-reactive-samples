package com.ra.intems.reactive.leaves.handler;

import com.ra.intems.reactive.leaves.dto.IntemsLeavesRequestDto;
import com.ra.intems.reactive.leaves.dto.IntemsLeavesResponseDto;
import com.ra.intems.reactive.leaves.service.IntemsUserLeavesService;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class LeavesHandlerFunction {

    private final IntemsUserLeavesService leavesService;

    public Mono<ServerResponse> create(ServerRequest request) {
        return request.bodyToMono(IntemsLeavesRequestDto.class)
                .flatMap(leavesService::createLeaves)
                .flatMap(p -> ServerResponse.ok().body(Mono.just(p), IntemsLeavesResponseDto.class));
    }

    public Mono<ServerResponse> getLeavesByIds(ServerRequest request) {
        List<Long> ids = request.queryParams().get("ids").
                stream().flatMap(id -> Stream.of(id.split(","))).map(Long::valueOf).collect(Collectors.toList());
        return defaultReadResponse(leavesService.getLeavesByUsersId(ids));
    }

    private Mono<ServerResponse> defaultReadResponse(Publisher<IntemsLeavesResponseDto> users) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(users, IntemsLeavesResponseDto.class);
    }
}
