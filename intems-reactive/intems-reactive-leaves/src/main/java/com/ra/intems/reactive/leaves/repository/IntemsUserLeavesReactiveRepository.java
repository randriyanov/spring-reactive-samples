package com.ra.intems.reactive.leaves.repository;

import com.ra.intems.reactive.leaves.entity.LeavesAndVacations;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.Collection;

public interface IntemsUserLeavesReactiveRepository extends ReactiveCrudRepository<LeavesAndVacations, Long> {

    Flux<LeavesAndVacations> findAllByUserIdIn(Collection<Long> userId);
}
