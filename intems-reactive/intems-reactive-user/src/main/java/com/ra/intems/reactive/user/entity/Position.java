package com.ra.intems.reactive.user.entity;

public enum Position {
    JAVA_SENIOR_SOFTWARE_ENGINEER("JAVA senior software engineer", 1),
    JAVA_MIDDLE_SOFTWARE_ENGINEER("JAVA middle software engineer", 2),
    JAVA_LEAD_SOFTWARE_ENGINEER("JAVA lead software engineer", 3);

    public final String positionName;
    public final int positionId;

    Position(String positionName, int positionId ) {
        this.positionName = positionName;
        this.positionId = positionId;
    }
}
