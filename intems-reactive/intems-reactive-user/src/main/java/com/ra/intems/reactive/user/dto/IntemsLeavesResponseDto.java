package com.ra.intems.reactive.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IntemsLeavesResponseDto {
    @JsonProperty("leaves_id")
    private Long leavesId;
    private int absence;
    @JsonProperty("vacation_days")
    private int vacationDays;
    @JsonProperty("sick_leaves")
    private int sickLeaves;
    @JsonProperty("user_id")
    private long userId;
}
