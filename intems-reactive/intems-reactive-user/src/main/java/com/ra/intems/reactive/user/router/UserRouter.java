package com.ra.intems.reactive.user.router;

import com.ra.intems.reactive.user.handler.UserHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class UserRouter {

    @Bean
    public RouterFunction<ServerResponse> routerAddress(UserHandlerFunction handlerFunction) {
        return route(POST("/user").and(accept(MediaType.APPLICATION_JSON)), handlerFunction::create)
                .andRoute(GET("/user/location/{locationAddress}").and(contentType(MediaType.APPLICATION_STREAM_JSON)), handlerFunction::getByLocation)
                .andRoute(GET("/user/name/{firstName}/{lastName}").and(contentType(MediaType.APPLICATION_STREAM_JSON)), handlerFunction::getByLastFirstName );
    }

}
