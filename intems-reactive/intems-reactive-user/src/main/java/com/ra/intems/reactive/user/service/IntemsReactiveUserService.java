package com.ra.intems.reactive.user.service;

import com.ra.intems.reactive.user.dto.IntemsUserRequestDto;
import com.ra.intems.reactive.user.dto.IntemsUserResponseDto;
import com.ra.intems.reactive.user.entity.IntemsUser;
import com.ra.intems.reactive.user.entity.Position;
import com.ra.intems.reactive.user.exception.UserNotFoundException;
import com.ra.intems.reactive.user.repository.UserReactiveRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
@RequiredArgsConstructor
public class IntemsReactiveUserService {

    private final UserReactiveRepository userRepository;

    @Transactional
    public Mono<IntemsUserResponseDto> responseDto(IntemsUserRequestDto requestDto) {
        IntemsUser intemsUser = new IntemsUser();
        intemsUser.setAboutMe(requestDto.getAboutMe());
        intemsUser.setFirstName(requestDto.getFirstName());
        intemsUser.setLastName(requestDto.getLastName());
        intemsUser.setOfficeLocation(requestDto.getOfficeLocation());
        intemsUser.setWorkEmail(requestDto.getWorkEmail());
        intemsUser.setPhoneNumber(requestDto.getPhoneNumber());
        intemsUser.setOfficeLocation(requestDto.getOfficeLocation());
        intemsUser.setPosition(Position.JAVA_LEAD_SOFTWARE_ENGINEER.positionId);
        return userRepository.save(intemsUser)
                .flatMap(id -> Mono.just(new IntemsUserResponseDto(intemsUser.getId(), requestDto.getFirstName(),
                requestDto.getLastName(), intemsUser.getPosition().positionName, requestDto.getOfficeLocation(),
                requestDto.getWorkEmail(), requestDto.getAboutMe(), requestDto.getPhoneNumber(), null)));
    }

    public Flux<IntemsUserResponseDto> getUsersByLocation(String location) {
        return userRepository.findAllByOfficeLocation(location)
                .map(this::mapToDto).switchIfEmpty(Mono.error(new UserNotFoundException("No users on this location")));
    }

    public Flux<IntemsUserResponseDto> getUsersByFirstNameLastName(String firstName, String lastName) {
        return userRepository.findTop100ByFirstNameOrLastName(firstName, lastName)
                .map(this::mapToDto).switchIfEmpty(Mono.error(new UserNotFoundException("No users found by name")));
    }

    private IntemsUserResponseDto mapToDto(IntemsUser intemsUser) {
        return new IntemsUserResponseDto(intemsUser.getId(), intemsUser.getFirstName(),
                intemsUser.getLastName(), Position.JAVA_LEAD_SOFTWARE_ENGINEER.positionName, intemsUser.getOfficeLocation(),
                intemsUser.getWorkEmail(), intemsUser.getAboutMe(), intemsUser.getPhoneNumber(), null);
    }
}
