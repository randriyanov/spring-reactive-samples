package com.ra.intems.reactive.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.stream.Stream;

@Table("items_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"workEmail"})
public class IntemsUser {
    @Id
    @Column("id")
    private Long id;
    @Column("first_name")
    private String firstName;
    @Column("last_name")
    private String lastName;
    @Column("position")
    private int position;
    @Column("office_location")
    private String officeLocation;
    @Column("work_email")
    private String workEmail;
    @Column("about_me")
    private String aboutMe;
    @Column("phone_number")
    private String phoneNumber;

    public Position getPosition() {
        return Stream.of(Position.values())
                .filter(p -> p.positionId == position)
                .findAny().orElseThrow(IllegalArgumentException::new);
    }

}
