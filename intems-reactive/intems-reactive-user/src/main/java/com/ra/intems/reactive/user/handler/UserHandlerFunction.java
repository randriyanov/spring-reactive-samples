package com.ra.intems.reactive.user.handler;

import com.ra.intems.reactive.user.dto.IntemsUserRequestDto;
import com.ra.intems.reactive.user.dto.IntemsUserResponseDto;
import com.ra.intems.reactive.user.service.IntemsReactiveUserService;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

@Component
@RequiredArgsConstructor
public class UserHandlerFunction {

    private final IntemsReactiveUserService userService;

    public Mono<ServerResponse> create(ServerRequest request) {
        return request.bodyToMono(IntemsUserRequestDto.class)
                .flatMap(userService::responseDto)
                .flatMap(p -> ServerResponse.ok().body(Mono.just(p), IntemsUserResponseDto.class));
    }

    public Mono<ServerResponse> getByLocation(ServerRequest request) {
        String path = request.pathVariable("locationAddress");
        return  defaultReadResponse(userService.getUsersByLocation(path)).publishOn(Schedulers.boundedElastic());
    }

    public Mono<ServerResponse> getByLastFirstName(ServerRequest request) {
        String firstName = request.pathVariable("firstName");
        String lastName = request.pathVariable("lastName");
        return  defaultReadResponse(userService.getUsersByFirstNameLastName(firstName, lastName))
                .publishOn(Schedulers.boundedElastic());
    }

    private Mono<ServerResponse> defaultReadResponse(Publisher<IntemsUserResponseDto> users) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(users, IntemsUserResponseDto.class);
    }
}
