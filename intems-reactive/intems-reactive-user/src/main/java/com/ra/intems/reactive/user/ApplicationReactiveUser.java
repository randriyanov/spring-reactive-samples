package com.ra.intems.reactive.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationReactiveUser {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationReactiveUser.class, args);
    }
}
