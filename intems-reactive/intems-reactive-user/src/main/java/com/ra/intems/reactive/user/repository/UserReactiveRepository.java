package com.ra.intems.reactive.user.repository;

import com.ra.intems.reactive.user.entity.IntemsUser;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface UserReactiveRepository extends ReactiveCrudRepository<IntemsUser, Long> {

    Flux<IntemsUser> findAllByOfficeLocation(String officeLocation);

    Flux<IntemsUser> findTop100ByFirstNameOrLastName(String firstName, String lastName);
}
