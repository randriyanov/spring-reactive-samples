package com.ra.intems.reactive.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationReactiveGateway {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationReactiveGateway.class, args);
    }
}
