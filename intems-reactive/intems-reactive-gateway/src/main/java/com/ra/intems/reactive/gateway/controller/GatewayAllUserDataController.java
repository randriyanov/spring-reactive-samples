package com.ra.intems.reactive.gateway.controller;

import com.ra.intems.reactive.gateway.service.GatewayUserService;
import com.ra.intems.reactive.gateway.dto.IntemsUserRequestDto;
import com.ra.intems.reactive.gateway.dto.IntemsUserResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;


@Controller
@RequestMapping("gateway")
@RequiredArgsConstructor
public class GatewayAllUserDataController {

    private final GatewayUserService userService;

    @PostMapping
    @ResponseBody
    public Mono<ResponseEntity<IntemsUserResponseDto>> createUser(@RequestBody IntemsUserRequestDto requestDto) {
        return userService.createUser(requestDto).flatMap(userResponse -> Mono.just(ResponseEntity.ok(userResponse)))
                .publishOn(Schedulers.boundedElastic());
                //.take(Duration.ofSeconds(6));
    }

    @GetMapping(value = "/location/{locationAddress}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseBody
    public Flux<IntemsUserResponseDto> getAllUsersByLocation(
            @PathVariable String locationAddress) {
        return userService.getUsersByMatchedLocation(locationAddress)
                .publishOn(Schedulers.boundedElastic());
                //.take(Duration.ofSeconds(6));
    }

    @GetMapping(value = "/name/{firstName}/{lastName}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseBody
    public Flux<IntemsUserResponseDto> getAllUsersWithMatchedNames(
            @PathVariable String firstName,
            @PathVariable String lastName) {
        return userService.getUsersByMatchedFirstLastName(firstName, lastName)
                .publishOn(Schedulers.boundedElastic());
                //.take(Duration.ofSeconds(6));
    }
}
