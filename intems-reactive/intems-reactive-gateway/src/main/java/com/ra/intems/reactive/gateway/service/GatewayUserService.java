package com.ra.intems.reactive.gateway.service;

import com.ra.intems.reactive.gateway.dto.IntemsLeavesRequestDto;
import com.ra.intems.reactive.gateway.dto.IntemsLeavesResponseDto;
import com.ra.intems.reactive.gateway.dto.IntemsUserRequestDto;
import com.ra.intems.reactive.gateway.dto.IntemsUserResponseDto;
import com.ra.intems.reactive.gateway.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GatewayUserService {

    private final WebClient userClient;
    private final WebClient leavesClient;

    public GatewayUserService(@Value("${user.service.url}") String userUrl,
                              @Value("${leaves.service.url}") String leavesUrl) {
        userClient = WebClient.builder().defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_STREAM_JSON_VALUE)
                .baseUrl(userUrl).build();
        leavesClient = WebClient.builder().defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_STREAM_JSON_VALUE)
                .baseUrl(leavesUrl).build();
    }

    public Mono<IntemsUserResponseDto> createUser(IntemsUserRequestDto requestDto) {
        return userClient.post().uri("/user").body(Mono.just(requestDto), IntemsUserResponseDto.class)
                .retrieve()
                .bodyToMono(IntemsUserResponseDto.class)
                .flatMap((Function<IntemsUserResponseDto, Mono<IntemsUserResponseDto>>) dto -> leavesClient.post()
                        .uri("/leaves")
                        .body(Mono.just(new IntemsLeavesRequestDto(dto.getId())), IntemsLeavesRequestDto.class)
                        .retrieve().bodyToMono(IntemsLeavesResponseDto.class)
                        .flatMap(leaves -> {
                            dto.setLeavesResponseDto(leaves);
                            return Mono.just(dto);
                        }));
    }

    public Flux<IntemsUserResponseDto> getUsersByMatchedLocation(String officeLocation) {
        return getUsersByLocation(officeLocation)
                .flatMap(userDto -> getUsersLeavesById(userDto).flatMap(leavesDto -> {
                    userDto.setLeavesResponseDto(leavesDto);
                    return Flux.just(userDto);
                }));
    }

    public Flux<IntemsUserResponseDto> getUsersByMatchedFirstLastName(String firstName, String lastName) {
        return getUsersByFirstLastName(firstName, lastName)
                .flatMap(userDto -> getUsersLeavesById(userDto).flatMap(leavesDto -> {
                    userDto.setLeavesResponseDto(leavesDto);
                    return Flux.just(userDto);
                }));
    }

    private Flux<IntemsUserResponseDto> getUsersByFirstLastName(String firstName, String lastName) {
        return userClient.get().uri(uriBuilder -> uriBuilder.path("/user/name/{firstName}/{lastName}")
                .build(firstName, lastName))
                .retrieve().bodyToFlux(IntemsUserResponseDto.class)
                .doOnError(throwable -> {
                    throw new UserNotFoundException("Users Not Found");
                });
    }

    private Flux<IntemsUserResponseDto> getUsersByLocation(String officeLocation) {
        return userClient.get().uri(uriBuilder -> uriBuilder.path("/user/location/{location}")
                .build(officeLocation))
                .retrieve().bodyToFlux(IntemsUserResponseDto.class)
                .doOnError(throwable -> {
                    throw new UserNotFoundException("Users Not Found");
                });
    }

    private Flux<IntemsLeavesResponseDto> getUsersLeavesById(IntemsUserResponseDto userResponseDtos) {
        return leavesClient.get().uri(uriBuilder -> uriBuilder.path("/leaves/user").queryParam("ids", "{ids}")
                .build(userResponseDtos.getId()))
                .retrieve().bodyToFlux(IntemsLeavesResponseDto.class)
                .onErrorReturn(new IntemsLeavesResponseDto());
    }

    private String userIds(List<IntemsUserResponseDto> userResponseDtos) {
        return userResponseDtos.stream()
                .map(IntemsUserResponseDto::getId).map(Object::toString)
                .collect(Collectors.joining(","));
    }

}
