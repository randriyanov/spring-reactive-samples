package com.ra.reactive.samples.intems.commons.repository.leaves;

import com.ra.reactive.samples.intems.commons.persist.leaves.LeavesAndVacations;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IntemsUserLeavesRepository extends JpaRepository<LeavesAndVacations, Long> {

    Optional<List<LeavesAndVacations>> findAllByUserIdIn(Collection<Long> userId);
}
