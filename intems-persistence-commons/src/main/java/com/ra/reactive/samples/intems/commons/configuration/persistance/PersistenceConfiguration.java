package com.ra.reactive.samples.intems.commons.configuration.persistance;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.dialect.PostgreSQL95Dialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Objects;

@Configuration
@EnableTransactionManagement
public class PersistenceConfiguration {


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                       Environment environment) {

        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setPackagesToScan(environment.getProperty("packages.scan"));
        emf.setDataSource(dataSource);
        emf.setJpaVendorAdapter(jpaVendorAdapter());
        return emf;
    }

    private JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);
        jpaVendorAdapter.setGenerateDdl(true);
        jpaVendorAdapter.setDatabasePlatform(PostgreSQL95Dialect.class.getName());
        return jpaVendorAdapter;
    }


    @Bean
    public DataSource dataSource(Environment environment) {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(environment.getProperty("db.username"));
        dataSource.setPassword(environment.getProperty("db.url"));
        dataSource.setJdbcUrl(environment.getProperty("db.password"));
        dataSource.setMinimumIdle(Integer.parseInt(Objects.requireNonNull(environment.getProperty("hikari.minimum-idle"))));
        dataSource.setMaximumPoolSize(Integer.parseInt(Objects.requireNonNull(environment.getProperty("hikari.maximum-pool-size"))));
        dataSource.setConnectionTimeout(Integer.parseInt(Objects.requireNonNull(environment.getProperty("hikari.connection-timeout"))));
        dataSource.setMaxLifetime(Integer.parseInt(Objects.requireNonNull(environment.getProperty("hikari.max-lifetime"))));
        dataSource.setIdleTimeout(Integer.parseInt(Objects.requireNonNull(environment.getProperty("hikari.idle-timeout"))));
        dataSource.setDriverClassName("org.postgresql.Driver");
        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
