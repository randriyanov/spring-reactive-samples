package com.ra.reactive.samples.intems.commons.persist.user;

import com.ra.reactive.samples.intems.commons.persist.Position;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "items_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"workEmail"})
public class IntemsUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Enumerated
    @Column(columnDefinition = "smallint")
    private Position position;
    @Column(name = "office_location")
    private String officeLocation;
    @Column(unique = true, name = "work_email")
    private String workEmail;
    @Column(name = "about_me")
    private String aboutMe;
    @Column(name = "phone_number")
    private String phoneNumber;

}
