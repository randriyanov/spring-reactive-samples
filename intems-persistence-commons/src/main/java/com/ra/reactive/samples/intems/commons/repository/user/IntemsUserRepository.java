package com.ra.reactive.samples.intems.commons.repository.user;

import com.ra.reactive.samples.intems.commons.persist.user.IntemsUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IntemsUserRepository extends JpaRepository<IntemsUser, Long> {

    Optional<List<IntemsUser>> findAllByOfficeLocation(String officeLocation);

    Optional<List<IntemsUser>> findTop100ByFirstNameOrLastName(String firstName, String lastName);
}
