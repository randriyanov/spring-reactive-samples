package com.ra.reactive.samples.intems.commons.persist.leaves;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "leaves_vacations")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"userId"})
public class LeavesAndVacations {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, name = "user_id")
    private Long userId;
    private int absence;
    @Column(name = "vacation_days")
    private int vacationDays;
    @Column(name = "sick_leaves")
    private int sickLeaves;
}
