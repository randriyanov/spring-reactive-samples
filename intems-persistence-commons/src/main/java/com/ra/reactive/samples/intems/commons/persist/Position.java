package com.ra.reactive.samples.intems.commons.persist;

public enum Position {
    JAVA_SENIOR_SOFTWARE_ENGINEER("JAVA senior software engineer"),
    JAVA_MIDDLE_SOFTWARE_ENGINEER("JAVA middle software engineer"),
    JAVA_LEAD_SOFTWARE_ENGINEER("JAVA lead software engineer");

    public final String positionName;

    Position(String positionName) {
        this.positionName = positionName;
    }
}
