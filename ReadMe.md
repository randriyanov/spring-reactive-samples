1. Start spring-mvc-tomcat
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/spring-mvc-tomcat/spring-mvc-tomcat.jmx -l log.jtl -e -o ./output-result

2. Start springboot-mvc
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc/springboot-mvc.jmx -l log.jtl -e -o ./output-result

3. Start springboot-mvc-future
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc-future/springboot-mvc-future.jmx -l log.jtl -e -o ./output-result

4. Start springboot-mvc-future-pool
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc-future-pool/springboot-mvc-future-pool.jmx -l log.jtl -e -o ./output-result

5. Start springboot-mvc-listenable future
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc-listenable/springboot-mvc-listenable.jmx -l log.jtl -e -o ./output-result

5. Start springboot-mvc-deferred
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc-deferred/springboot-mvc-deferred.jmx -l log.jtl -e -o ./output-result

6. Start springboot-mvc-completable
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-mvc-completable/springboot-mvc-completable.jmx -l log.jtl -e -o ./output-result

7. Start springboot-reactive-sequential
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-reactive/springboot-reactive.jmx -l log.jtl -e -o ./output-result

8. Start springboot-reactive-streams
/Users/randriinov/Downloads/apache-jmeter-5.3/bin/jmeter.sh -n -t jmeter/springboot-reactive-streams/springboot-reactive-streams.jmx -l log.jtl -e -o ./output-result

jmeter.sh -n -t loadtest.jmx -l log.jtl -e -o ./output-result

jmeter.sh -n -t spring-mvc-tomcat-get.jmx -l log.jtl -e -o ./spring-mvc-tomcat-get-output